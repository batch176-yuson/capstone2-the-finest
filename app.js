//[SECTION] Dependencies and Modules
	const express = require('express');
	const mongoose = require('mongoose');
	const dotenv = require('dotenv');
	const userRoutes = require('./routes/users');
	const productRoutes = require('./routes/products');
	const orderRoutes = require('./routes/orders');
	const cors = require('cors');

//[SECTION] Environment Setup
	dotenv.config();
	let account = process.env.CREDENTIALS;
	const port = process.env.PORT;

//[SECTION]	Server Setup
	const app = express();
	app.use(express.json());
	app.use(cors());
	app.use(express.static(__dirname))

//[SECTION]	Database Connection
	mongoose.connect(account);
	const connectStat = mongoose.connection;
	connectStat.once('open', () => console.log('Database Connected'));

//[SECTION]	Backend Routes
	app.use('/users',userRoutes);
	app.use('/products', productRoutes);
	app.use('/orders', orderRoutes);

//[SECTION]	Server Gateway Response
	app.get('/', (req,res) => {
		res.send(`Welcome to The Finest!`);
	});
	app.listen(port, () => {
		console.log(`API is Hosted on port ${port}`);
	});