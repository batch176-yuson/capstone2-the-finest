//[SECTION] Dependencies and Modules
	const User = require('../models/Users');
	const Product = require('../models/Products'); 
	const bcrypt = require('bcrypt'); 
	const dotenv = require('dotenv');
	const auth = require('../auth.js');


//[SECTION]Environment Variable Setup
	dotenv.config()
	const asin = parseInt(process.env.SALT);

//[SECTION] Functionalities [CREATE]
	//1.Register New Account
		module.exports.register = (userData) => {
			let email = userData.email;
			let passw = userData.password;
		
				let newUser = new User({
				email: email,
				password: bcrypt.hashSync(passw, asin)
			});
			
			return newUser.save().then((user,err) => {
				if (user) {
					return user;
				} else {
					return 'Failed to Register account';
				}
			});
		};

//[SECTION] User Authentication
		module.exports.loginUser = (data) => {

			return User.findOne({ email: data.email }).then(result => {
				
				if (result == null){
					return false;
				} else {

					const isPasswordCorrect = bcrypt.compareSync(data.password, result.password);

					if (isPasswordCorrect) {
						return {accessToken: auth.createAccessToken(result.toObject())}
					} else {
						return false;
					}
				}
			})
		}

//[SECTION] Functionalities [RETRIEVE] the user details

	module.exports.getProfile = (data) => {
		return User.findById(data).then(result => {
			//change the value of the user's password to an empty string
			result.password = " ";
			return result;
		})
	};	

//[SECTION] Adding order
		module.exports.order = async (req, res) => {
			if(req.user.isAdmin) {
				return res.send({ message: "Action Forbidden"})
			}

			let isUserUpdated = await User.findById(req.user.id).then( user => {

				let newOrder = {
						productName: req.body.productName,
						shoeSize: req.body.shoeSize,
						productId: req.body.productId,
						quantity: req.body.quantity,
						totalAmount: req.body.totalAmount
					}
						user.orders.push(newOrder);
						return user.save().then(user => true).catch(err => err.message)

							if(isUserUpdated !== true) {
								return res.send({ message: isUserUpdated })
							}
				});

				let isOrderUpdated = await Product.findById(req.body.productId).then(product => {

						let order = {
							productId: req.body.productId
						}

							product.orders.push(order);
							return product.save().then(product => true).catch(err => err.message)


							if(isOrderUpdated !== true) {
									return res.send({ message: isOrderUpdated })
							}

				});

				if(isUserUpdated && isOrderUpdated) {
					return res.send({ message: "Ordered Successfully"})
				}

		}	

		module.exports.getOrders = (req, res) => {
				User.findById(req.user.id)
				.then(result => res.send(result.orders))
				.catch(err => res.send(err))
		}
//[SECTION] Set user as admin
	module.exports.setAdmin = (isAdmin) => {
		let toAdmin = {
			isAdmin: true
		};

		return User.findByIdAndUpdate(isAdmin, toAdmin).then((user,error) => {
			if (error) {
				return false;
			} else {
				return true;
			}
		}).catch(error => error);
	};
