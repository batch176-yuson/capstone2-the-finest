//[SECTION] Dependencies and Modules
	const Product = require('../models/Products');  
//[SECTION]Environment Variable Setup

//[SECTION] Functionalities [CREATE]
	//1.Add Product
	module.exports.addProduct = (reqBody, reqFile, url) => {
		let newProduct = new Product({
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price,
			image: url + "/images/" +  reqFile.filename
		});
			
		return newProduct.save().then((product, error) => {
			if (error) {
				return error;
			} else {
				return product;
			}
		}).catch(error => error);

	};

//[SECTION] Functionalities [RETRIEVE]
	//Retrieve all products from the DB
	module.exports.getAllProducts = () => {
		return Product.find({}).then(result => {
			return result;
		});
	};

	//Retrieve a products
	module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams).then(result => {
		return result
	}).catch(error => error);
};

//[SECTION] Functionalities [UPDATE]
	//update a product
	module.exports.updateProduct = (productId, data) => {
		let updatedProduct = {
			name: data.name,
			description: data.description,
			price: data.price
		};

		return Product.findByIdAndUpdate(productId, updatedProduct).then((product, error) => {
			if (error) {
				return false;
			} else {
				return true;
			}
		}).catch(error => error);
	};

	//Activate a product
	module.exports.activateProduct = (productId) => {
		let updateActiveField = {
			isActive: true
		};

		return Product.findByIdAndUpdate(productId, updateActiveField).then((product, error) => {
				if(error){
					return false;
				} else {
					return true;
				}
			}).catch(error => error)
		}
	
//[SECTION] Functionalities [DELETE]
	//Archiving a product
	module.exports.archiveProduct = (productId) => {
		let updateActiveField = {
			isActive: false
		};

		return Product.findByIdAndUpdate(productId, updateActiveField).then((product, error) => {
				if(error){
					return false;
				} else {
					return true;
				}
			}).catch(error => error)
		}