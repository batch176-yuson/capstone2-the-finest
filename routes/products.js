//[SECTION] Dependencies and Modules
	const exp = require('express');
	const ProductController = require('../controller/products');
	const auth = require('../auth');

//Destructure the actual function that we need to use.
	const {verify, verifyAdmin} = auth;

//[SECTION]	Routing Component
	const route = exp.Router();
	const multer = require('multer')
	const path = require('path')


	const storage = multer.diskStorage({
	    destination: function (req, file, callback) {
	        callback(null, path.join(__dirname, '../images/'))
	    },
	    filename: function(request, file, callback){
	        callback(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
	    }
	})

	const upload = multer({
	    storage: storage,
	    fileFilter: (req, file, cb) => {
	        if (file.mimetype == "image/png" || file.mimetype == "image/jpg" || file.mimetype == "image/jpeg") {
	            cb(null, true);
	        } else {
	            cb(null, false);
	            return cb(new Error('Only .png, .jpg and .jpeg format allowed!'));
	        }
	    }
	})

//[SECTION] Routes-POST
	route.post('/add', verify, verifyAdmin, upload.single('image'), (req, res) => {
		const url = req.protocol + '://' + req.get('host')
		ProductController.addProduct(req.body, req.file, url).then(result => res.send(result));

		
	});


//[SECTION] Routes-GET
	//retrieve all products
	route.get('/all', (req, res) => {
		ProductController.getAllProducts().then(result => res.send(result));
	});

	//retrieve a specific product 
	route.get('/:productId', (req, res) => {
		console.log(req.params.productId)
		ProductController.getProduct(req.params.productId).then(result => res.send(result));
	});

//[SECTION] Routes-PUT
	//update a product
	route.put('/:productId', verify, verifyAdmin, (req, res) => {
		ProductController.updateProduct(req.params.productId, req.body).then(result => res.send(result));
	});

//[SECTION] Routes-DEL
	//Archiving a product
	route.put('/:productId/archive', verify, verifyAdmin, (req,res) => {ProductController.archiveProduct(req.params.productId).then(result => res.send(result));
	});

	//Activate a product
	route.put('/:productId/activate', verify, verifyAdmin, (req,res) => {ProductController.activateProduct(req.params.productId).then(result => res.send(result));
	});

//[SECTION] Expose Route System
	module.exports = route;