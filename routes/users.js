//[SECTION] Dependencies and Modules
	const exp = require('express');
	const controller = require('../controller/users');
	const auth = require('../auth.js');
	const User = require('../models/Users');


//[SECTION]	Routing Component
	const {verify, verifyAdmin} = auth;
	const route = exp.Router();

//[SECTION] Routes-POST
	route.post('/register', async (req, res) => {

		const existEmail = await User.findOne({ email: req.body.email})
		
		if(!existEmail){
			controller.register(req.body).then(result => res.send(result))

		}else res.send ({"message": "Email already exists. Do you with to login?"})

		});

//[SECTION] Routes for User Authentication(login)
	route.post('/login',(req,res) => {
		controller.loginUser(req.body).then(result => res.send(result));
	});

//[SECTION] Routes-GET the user's details
	route.get('/details', auth.verify, (req,res) => {
		controller.getProfile(req.user.id).then(result => res.send(result));
	});

//[SECTION] Routes-PUT
	//Set user as admin
	route.put('/:userIdToAdmin', verify, verifyAdmin,  (req,res) => {controller.setAdmin(req.params.userIdToAdmin).then(result => res.send(result));

		});

	module.exports = route;