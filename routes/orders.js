//[SECTION] Dependencies and Modules
	const exp = require('express');
	const auth = require('../auth.js');
	const OrderController = require('../controller/orders');

//[SECTION] Destructuring of Auth functions from auth.js
	const {verify, verifyAdmin} = auth;
	const route = exp.Router();

//[SECTION] ROUTES - POST
		//------------------Create an Order
		route.post('/createorder', verify, OrderController.CREATEORDER);

//[SECTION] ROUTES - GET
		//Retrieve all orders  (Admin only)
		route.get('/allorders', (req, res) => {
			OrderController.GETALLORDERS().then(result => res.send(result));
		})
		//Get specific orders (By User)
		route.get('/getmyorders', verify, OrderController.GETMYORDERS);

module.exports = route;
