//[SECTION] Modules and Dependencies	
	const mongoose = require('mongoose');

//[SECTION]	Schema/Blueprint
	const productSchema = new mongoose.Schema({
		name: {
			type: String,
			required: [true, 'Product name is required']
		},
		description: {
			type: String,
			required: [true, 'Product description is required']
		},
		price: {
			type: Number,
			required: [true, 'Product price is required']
		},

		image: {
			type: String,
			required: [true, 'Product image is required']
		},
	
		isActive: {
			type: Boolean,
			default: true
		},
		createdOn: {
			type: Date,
			default: new Date()
		}
	
	});

//[SECTION]	Model
	module.exports = mongoose.model('Product', productSchema);

	