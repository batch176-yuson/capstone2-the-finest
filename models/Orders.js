//[SECTION] Dependencies and Modules
  const mongoose = require('mongoose');
  const Product = require('../models/Products');

//[SECTION] Schema/Blueprint --------- Orders Schema
const orderSchema = new mongoose.Schema({
   userId: {
      type: String,
      ref: 'User',
       required: [true, 'User ID is Required'] 
    },
   products: [
      {
         productId: {
            type: mongoose.Schema.Types.ObjectId, 
            ref: 'Product',
            required: [true, 'Product ID is Required']
          },
         quantity: {
            type: Number,
            default: 1
         }
      }
   ], 
   totalAmount: {
      type: Number,
   },
   purchasedOn: {
      type: Date,
      default: new Date()
   }
});



//[SECTION] Model
	module.exports = mongoose.model('Order', orderSchema);

