//[SECTION] Modules and Dependencies	
	const mongoose = require('mongoose');

//[SECTION]	Schema/Blueprint
	const userSchema = new mongoose.Schema({
		email: {
			type: String,
			required: [true, 'Email is Required'] 
		},
		password: {
			type: String,
			required: [true, 'Password is Required'] 
		},
		isAdmin: {
			type: Boolean,
			default: false
		},
		orders: [
				{    
			    productName: {
			        type: String,
			        required: [true, 'Product Name is required']
			    },
			    shoeSize: {
			    	type: String,
			    	required: [true, 'Shoe Size is required']
			    },
			    quantity: {
			        type: Number,
			        required: [true, 'Product Quantity is requred']
			    },
			   	totalAmount: {
			            type: Number,
			            required: [true, 'Total Amount is required']
			    },
			    purchasedOn: {
			            type:Date,
			            default: new Date()
		        }
		    }	
		]
	});

//[SECTION]	Model
	module.exports = mongoose.model('User', userSchema);

	